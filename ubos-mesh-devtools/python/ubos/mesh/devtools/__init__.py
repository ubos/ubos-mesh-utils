#!/usr/bin/python
#
# Copyright (C) Johannes Ernst. All rights reserved. License: see package.
#

import argparse
import importlib
import glob
import os
from pathlib import Path
import re
import sys
import ubos.mesh.devtools.commands
import ubos.logging
import ubos.utils


def run():
    """
    Main entry point: looks for available subcommands and
    executes the correct one.
    """
    cmdNames = ubos.utils.findSubmodules(ubos.mesh.devtools.commands)

    parser = argparse.ArgumentParser( description='UBOS Mesh development tools).')
    parser.add_argument('-v', '--verbose', action='count',       default=0,  help='Display extra output. May be repeated for even more output.')
    parser.add_argument('--logConfig',                                       help='Use an alternate log configuration file for this command.')
    parser.add_argument('--debug',         action='store_const', const=True, help='Suspend execution at certain points for debugging' )
    cmdParsers = parser.add_subparsers( dest='command', required=True )

    cmds = {}
    for cmdName in cmdNames:
        mod = importlib.import_module('ubos.mesh.devtools.commands.' + cmdName)
        mod.addSubParser( cmdParsers, cmdName )
        cmds[cmdName] = mod

    args,remaining = parser.parse_known_args(sys.argv[1:])
    cmdName = args.command

    ubos.logging.initialize('mesh-dev', cmdName, args.verbose, args.logConfig, args.debug)

    if cmdName in cmdNames:
        try :
            ret = cmds[cmdName].run( args, remaining )
            exit( ret )

        except Exception as e:
            ubos.logging.fatal( str(type(e)), '--', e )

    else:
        ubos.logging.fatal('Sub-command not found:', cmdName, '. Add --help for help.' )


def runBuild( args, chapters ) :
    """
    Run a build
    """

    toBuild = _determinePackages( chapters, args.fromChapter, args.afterChapter, args.skipChapter, args.config )

    built = []
    for p in toBuild :
        print( 'Creating package %s' % p )
        cmd = 'cd %s' % p
        cmd += ' &&'
        if args.nocompress :
            cmd += ' PKGEXT=.pkg.tar'
            ext = ''
        else :
            ext = '.xz'

        cmd += ' makepkg -f -i --noconfirm -s' # -d gets in the way of -s

        if args.dryRun :
            print( cmd )
        else :
            ( ret, stdout, stderr ) = ubos.utils.myexec( cmd, None, True, True )

            if stdout is not None:
                stdout = stdout.decode("utf-8")

            if stderr is not None:
                stderr = stderr.decode("utf-8")

            if ret != 0 :
                ubos.logging.fatal( 'Build failed:',  stdout, stderr )

            m = re.search( 'Finished making: (\S+) (\S+)', stdout )
            if m :
                built.append( '%s/%s-%s*.pkg.tar%s' % ( p, m.group(1), m.group(2), ext ))
            else :
                ubos.logging.error( "Failed to find name of built package ", p )

    if args.pushto :
        _pushTo( args.pushto, identityFile, built, args.dryRun )


def runList( args, chapters ) :
    """
    List the build activities
    """

    toBuild = _determinePackages( chapters, args.fromChapter, args.afterChapter, args.skipChapter, args.config )

    built = []
    for p in toBuild :
        print( 'Package %s' % p )


def runClean( args, chapters ) :
    """
    Clean all build artifacts.
    NOTE: This does not clean ~/.m2/repository
    """

    toClean = _determinePackages( chapters, args.fromChapter, args.afterChapter, args.skipChapter, args.config )

    for p in toClean :
        print( '\nCleaning package %s' % p )

        cmd = 'cd %s &&' % p
        if Path( '%s/JV' % p ).exists() :
            cmd += ' JAVA_HOME=/usr/lib/jvm/java-$(cat JV)-openjdk'
        cmd += ' gradle clean'

        if args.dryRun :
            print( cmd )

        else :
            if ubos.utils.myexec( cmd ) != 0 :
                ubos.logging.fatal( 'gradle clean failed' )

        toDelete = glob.glob( '%s/%s-*.pkg*' % ( p, p ) )
        if args.dryRun :
            print( 'Deleting file %s' % toDelete )

        else :
            ubos.utils.deleteFile( *toDelete )


def runTest( args, toRun ) :
    """
    Run some tests
    """

    toRun = _determineTestModules( toRun, args.fromModule, args.afterModule, args.skipModule, args.config )

    for p in toRun :
        cmd = 'diet4j %s' % p
        print( 'Running test: %s' % cmd )

        if not args.dryRun :
            if ubos.utils.myexec( cmd ) != 0 :
                ubos.logging.fatal( 'test', p, 'failed' )
            print()


def _pushTo( host, identityFile, packages, dryRun ) :
    """
    ubos-push the named packages to the named host
    """

    cmd  = 'ubos-push'
    cmd += ' --verbose'
    if identityFile :
        cmd += ' -i %s' % identityFile
    cmd += '--host %s %s' % ( host, ' '.join( packages ))

    print( cmd )
    if not dryRun :
        if ubos.utils.myexec( cmd ) != 0 :
            ubos.logging.fatal( 'ubos-push failed' )


def _determinePackages( chapters, fromChapter, afterChapter, skipChapter, configFile ) :
    """
    Helper method to convert a list of chapter names to a list of package names, given the
    config file. There are now two situations:
    * the current directory contains a config file
    * the current directory does not contain a config file, but subdirectories do
    """

    ret = []
    if Path( configFile ).exists() :
        # current directory contains config file
        configJson = ubos.utils.readJsonFromFile( configFile )

        if not 'chapters' in configJson :
            ubos.logging.fatal( 'No "chapters" section in config file', configFile )

        if 'chapterseq' in configJson :
            chapterseq = configJson['chapterseq']
        else :
            chapterseq = []

        chaptersFound = configJson['chapters'] # make a copy that can be destroyed
        if chapters is None or len( chapters ) == 0 :
            chapters = chaptersFound

        for name in chapterseq :
            if name in chapters and name in chaptersFound : # be robust
                ret += chaptersFound[name]
                del chaptersFound[name]

        for name in chapters : # remainder not specified in chapterseq
            if name in chaptersFound :
                ret += chaptersFound[name]

    else :
        # "chapters" in this case can be chapters like "ubos-mesh/base" or repos like "ubos-mesh"
        configJsons  = {}
        buildSeqDict = {}
        for p in Path( '.' ).glob( '*/' + configFile ):
            pName                 = os.fspath( p )
            pDirName              = pName[ 0 : -len( configFile ) - 1 ]
            configJsons[pDirName] = ubos.utils.readJsonFromFile( pName )
            if 'chapters' in configJsons[ pDirName ] :
                if 'buildseq' in configJsons[ pDirName ] :
                    buildSeqDict[ pDirName ] = configJsons[ pDirName ]['buildseq']
            else :
                del configJsons[pDirName] # skip

        if chapters is None or len( chapters ) == 0 :
            chapters = configJsons.keys() # all repos

        # replace the repos with contained chapters
        realChapters = []
        for chapter in chapters :
            if chapter.find( '/' ) >= 0 :
                realChapters += [ chapter ];
            else :
                for c in configJsons[ chapter ]['chapters'] :
                    realChapters += [ "%s/%s" % ( chapter, c ) ]

        def _chapterOrderIndex( c ) :
            repo, chapter = c.split( '/' )
            if repo in buildSeqDict :
                if 'chapterseq' in configJsons[repo] and chapter in configJsons[repo]['chapterseq']:
                    return ( buildSeqDict[ repo ], configJsons[repo]['chapterseq'].index( chapter ))
                else :
                    return ( buildSeqDict[ repo ], 0 )

            else :
                return ( 9999, 9999 ) # last


        chapters = sorted( realChapters, key=_chapterOrderIndex )

        for name in chapters :
            repo, chapter = name.split( '/', maxsplit=1 )
            for package in configJsons[repo]['chapters'][chapter] :
                ret += [ "%s/%s" % ( repo, package ) ]

    if fromChapter is not None and len( fromChapter ) > 0 :
        for i in range( len( ret )) :
            if ret[i].startswith( fromChapter + '/' ) or ret[i] == fromChapter :
                ret = ret[ i : ]
                break

    elif afterChapter is not None and len( afterChapter ) > 0 :
        found = False
        for i in range( len( ret )) :
            if ret[i].startswith( afterChapter + '/' ) or ret[i] == afterChapter :
                found = True
            elif found :
                ret = ret[ i : ]
                break;

    if skipChapter is not None and len( skipChapter ) > 0 :
        for skip in skipChapter :
            ret = [ keep for keep in ret if not keep.startswith( skip ) ]

    return ret


def _determineTestModules( toRun, fromModule, afterModule, skipModule, configFile ) :
    """
    Helper method to convert a list of test module names to a list of existing test modules, given the
    config file
    """

    if Path( configFile ).exists() :
        configJson = ubos.utils.readJsonFromFile( configFile )

        if not 'testModules' in configJson :
            ubos.logging.fatal( 'No "testModules" section in config file', configFile )

        if len( toRun ) > 0 :
            for t in toRun :
                if not t in configJson['testModules'] :
                    ubos.logging.fatal( 'Unknown test module:', t )

        else :
            ret = configJson['testModules']

    else :
        ret = []
        if toRun is None or len( toRun ) == 0 :
            configJsons  = {}
            buildSeqDict = {}
            for p in Path( '.' ).glob( '*/' + configFile ):
                pName                 = os.fspath( p )
                pDirName              = pName[ 0 : -len( configFile ) - 1 ]
                configJsons[pDirName] = ubos.utils.readJsonFromFile( pName )
                if 'testModules' in configJsons[ pDirName ] :
                    if 'buildseq' in configJsons[ pDirName ] :
                        buildSeqDict[ pDirName ] = configJsons[ pDirName ]['buildseq']
                else :
                    del configJsons[pDirName] # skip

                reposToRun = list( configJsons.keys()) # all repos

            if fromModule is not None and len( fromModule ) > 0 :
                for i in range( len( reposToRun )) :
                    if reposToRun[i].startswith( fromModule + '/' ) or reposToRun[i] == fromModule :
                        reposToRun = reposToRun[ i : ]
                        break

            elif afterModule is not None and len( afterModule ) > 0 :
                found = False
                for i in range( len( reposToRun )) :
                    if reposToRun[i].startswith( afterModule + '/' ) or reposToRun[i] == afterModule :
                        found = True
                    elif found :
                        reposToRun = reposToRun[ i : ]
                        break;

            if skipModule is not None and len( skipModule ) > 0 :
                for skip in skipModule :
                    reposToRun = [ keep for keep in ret if not keep.startswith( skip ) ]

            def _reposOrderIndex( repo ) :
                if repo in buildSeqDict :
                    return buildSeqDict[ repo ]
                else :
                    return 9999

            reposToRun = sorted( reposToRun, key=_reposOrderIndex )
            for repo in reposToRun :
                if 'testModules' in configJsons[repo] :
                    ret += configJsons[repo]['testModules']

        else :
            for t in toRun :
                if Path( '%s/%s' %( t, configFile )).exists() :
                    configJson = ubos.utils.readJsonFromFile( '%s/%s' % ( t, configFile ))
                else :
                    ubos.logging.fatal( 'Cannot run tests in repo, config file does not exist:', '%s/%s' % ( t, configFile ))

                if 'testModules' in configJson:
                    ret += configJson['testModules']
                else :
                    ubos.logging.fatal( 'No testModules in: ', '%s/%s'% ( t, configFile ))

    return ret
