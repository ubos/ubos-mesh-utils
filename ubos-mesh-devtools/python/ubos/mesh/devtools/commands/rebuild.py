#!/usr/bin/python
#
# Run a clean build
#
# Copyright (C) Johannes Ernst. All rights reserved. License: see package.
#

import ubos.mesh.devtools


def run( args, remainder ) :
    """
    Run this command.
    """

    ubos.mesh.devtools.runClean( args, remainder )
    ubos.mesh.devtools.runBuild( args, remainder )


def addSubParser( parentParser, cmdName ) :
    """
    Enable this command to add its own command-line options
    parentParser: the parent argparse parser
    cmdName: name of this command
    """
    parser = parentParser.add_parser( cmdName,                                             help='Run a UBOS Mesh development build.' )
    parser.add_argument('--config',       default="devtools.json",                         help='Configuration file. Defaults to devtools.json.' )
    parser.add_argument('--pushto',                                                        help='Host to ubos-push build artifact to.' )
    parser.add_argument('-i',                                                              help='Identity file for ubos-push' )
    parser.add_argument('--nocompress',   action='store_const', const=True,                help='Do not compress the package (faster).' )
    parser.add_argument('-dry-run', '-n', action='store_const', const=True, dest='dryRun', help='Do not execute, just print' )

    fromOrAfterGroup = parser.add_mutually_exclusive_group( required=False )
    fromOrAfterGroup.add_argument('--from',  dest="fromChapter",                   help='Name of the repo or repo/chapter to start from' )
    fromOrAfterGroup.add_argument('--after', dest="afterChapter",                  help='Name of the repo or repo/chapter to start after' )
    fromOrAfterGroup.add_argument('--skip',  dest="skipChapter",  action="append", help='Name of repos or repo/chapters to skip' )
