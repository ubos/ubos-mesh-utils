#!/usr/bin/python
#
# Run tests
#
# Copyright (C) Johannes Ernst. All rights reserved. License: see package.
#

import ubos.mesh.devtools


def run( args, remainder ) :
    """
    Run this command.
    """

    ubos.mesh.devtools.runTest( args, remainder )


def addSubParser( parentParser, cmdName ) :
    """
    Enable this command to add its own command-line options
    parentParser: the parent argparse parser
    cmdName: name of this command
    """
    parser = parentParser.add_parser( cmdName,                                             help='Run UBOS Mesh tests.' )
    parser.add_argument('--config',       default="devtools.json",                         help='Configuration file. Defaults to devtools.json.' )
    parser.add_argument('-dry-run', '-n', action='store_const', const=True, dest='dryRun', help='Do not execute, just print' )

    fromOrAfterGroup = parser.add_mutually_exclusive_group( required=False )
    fromOrAfterGroup.add_argument('--from',  dest="fromModule",                   help='Name of the test module to start from' )
    fromOrAfterGroup.add_argument('--after', dest="afterModule",                  help='Name of the test module to start after' )
    fromOrAfterGroup.add_argument('--skip',  dest="skipModule",  action="append", help='Name of test modules to skip' )
