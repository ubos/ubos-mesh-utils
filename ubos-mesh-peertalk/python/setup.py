#!/usr/bin/python
#
# Setup the package.
#
# Copyright (C) Johannes Ernst. All rights reserved. License: see package.
#

from pathlib import Path
from setuptools import setup
import ubos.mesh.peertalk

setup(name='ubos-mesh-peertalk',
      version=Path('../PKGVER').read_text().strip(),
      author='Johannes Ernst',
      license='AGPLv3',
      description='Python command-line utility for Peertalk',
      url='https://gitlab.com/ubos/ubos-mesh-peertalk',
      packages=[
          'ubos.mesh.peertalk',
          'ubos.mesh.peertalk.commands'
      ],
      zip_safe=False)
