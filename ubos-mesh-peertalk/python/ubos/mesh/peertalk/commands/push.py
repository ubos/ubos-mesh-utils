#!/usr/bin/python
#
# Push a PeerTalk message
#
# Copyright (C) Johannes Ernst. All rights reserved. License: see package.
#

import argparse
import requests
import ubos.logging
import ubos.utils

def run( args ) :
    """
    Run this command.
    """

    content = ubos.utils.slurpFile( args.input )

    ubos.logging.trace( 'Posting', len(content), 'to', args.endpoint )

    headers={
        'Content-Type' : 'application/json'
    }
    if args.bearertoken :
        headers['Authorization'] = 'Bearer ' + args.bearertoken

    response = requests.post( args.endpoint, data=content, headers=headers )

    ubos.logging.trace( 'Response:', response )

    if response.status_code == 202:
        print( 'Success' )
    else :
        ubos.logging.error( 'Upload failed with HTTP:', response.status_code, "\n" + response.text )


def addSubParser( parentParser, cmdName ) :
    """
    Enable this command to add its own command-line options
    parentParser: the parent argparse parser
    cmdName: name of this command
    """
    parser = parentParser.add_parser( cmdName,                           help='Push a PeerTalk message to a PeerTalk handler.' )
    parser.add_argument( '--input',       required=True,                 help='Name of the file containing the message' )
    parser.add_argument( '--endpoint',    default='http://localhost/pt', help='URL of the PeerTalk endpoint (default is http://localhost/pt)' )
    parser.add_argument( '--bearertoken',                                help='Bearer token for authorization' )
