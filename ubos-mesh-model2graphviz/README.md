# How To

You need a relatively recent python V3 installed on your system.

If you are running Linux use the package manager, if it's MacOS use brew.

Read this: https://docs.python.org/3/installing/index.html

The best way to run a python app is to install a python "venv" or virtual environment
in this directory.

Do this by first making sure you have pip installed (hint: read the link)

On MacOS for example:

```
  brew upgrade  
  python3 -m pip install --user --upgrade pip  
  python3 -m pip --version
  python3 -m pip install --user virtualenv  
  python3 -m venv env
  source env/bin/activate
  which python
```

Install graphviz:

```
  brew install graphviz
```

Install the required python module:

```
  python3 -m pip install -r requirements.txt
```

Then try running this script on a test model.xml file, e.g.

```
  python model2graphviz.py ../../ubos-mesh/ubos-mesh-test-model-library/net.ubos.model.Test/src/main/models/model.xml
``` 

For more "representative" picture:

```
   python model2graphviz.py ../../ubos-mesh/ubos-mesh-model-library/net.ubos.model.Person/src/main/models/model.xml
```
To leave:

```
  deactivate
```

