# Copyright (c) 2022, Adrian Blakey. All rights reserved.

import os
import xml.etree.ElementTree as ET
import graphviz
import argparse

# Render a Ubos-Net model.xml file as a diagram using graphviz
# Usage: model2graph [path_to_a_model.xml_file_or_the_model.xml_in_this_directory]

# XML parse the model.xml file.
# Create a "dot" digraph for this SA when it's found.
# Cache the names of all dependent SA - not really necessary
# Write the ET's in this subject area immediately.
# Cache all relationships including supertype - output at the end.
# Cache all other ET's that are in other subject areas, in a set mapped to the SA name.
# At end - iterate through SA's and output the SA's with contained ET's.
# Output all edges/RT.
# Notes:
# Store a reln as a set of tuples: src, dest, srcmult, destmult
# Store subgraphs as dict of SA key, set of contained ET's
# Globals for dicts, rendering control and the "dot" of this SA digraph.

# Author: Adrian Blakey adrianblakey@gmail.com
# TODO: more testing, options for rendering the output in different ways,
# TODO: render it a bit more like je does

# Globals
dot: graphviz.Digraph = None             # Graphname
sa_id: str = None                        # This SA identifier
et_names_set = set()                     # All the ets/nodes that are rendered so we don't render more than once
sa_names_set = set()                     # Set of sa names on which this model depends
reln_dict = dict()                       # Map reln name to the reln tuple
cluster_node_set_dict = dict()           # Key - name of SA, value set of ET's
# Global rendering consts.
node_shape = 'rectangle'                 # Nodes as boxes
node_fontname = 'Courier'                # To get the spacing right

edge_select = ["n","ne","e","se","s","sw","w","nw","c","_"]

# Parse out the SA ID from the <subjectarea ID=
# and create the parent cluster object "dot"
def do_sa_id(the_sa: ET.Element) -> None:
    global dot
    global sa_id
    global node_shape
    global node_fontname
    the_sa_attribs = the_sa.attrib
    for attrib, value in the_sa_attribs.items():
        if attrib == 'ID':
            sa_id = value
            break
    if sa_id is None:
        print('Error: missing subject area')
        exit()
    a_lab = '    label = "' + sa_id + '";\n'                      # Label the main SA graph
    dot = graphviz.Digraph(name=sa_id, comment='Subject Area ID: ' + sa_id,
                           node_attr={'shape': node_shape, 'fontname': node_fontname},
                           graph_attr={'compound': 'true', 'rankdir': 'NS'},
                           body=[a_lab])


# Process the "depends on" by caching the SA name - probably not needed ...
def do_depends_on(depend_element: ET.Element) -> None:
    global dot
    global sa_names_set
    for deps_on in depend_element:
        if deps_on.tag != 'subjectareareference':
            print('Error: expecting the tag "subjectareareference" - found: ' + deps_on.tag)
            exit(6)
        for sa_refs in deps_on:
            if sa_refs.tag != 'name':
                print('Error: expecting the tag "name" - found: ' + sa_refs.tag)
                exit(6)
            # Name of a SA on which we depend
            sa_ref_name = sa_refs.text
            sa_names_set.add(sa_ref_name)


# Process a PT. Add it to a PT map for this ET.
def do_property_type(et: ET.Element, property_dict: dict) -> None:
    pt_is_opt = ''
    for pt in et:
        if pt.tag == 'name':
            pt_name = pt.text
        elif pt.tag == 'username':
            pt_username = pt.text
        elif pt.tag == 'userdescription':
            pt_userdesc = pt.text
        elif pt.tag == 'defaultvalue':
            pass
        elif pt.tag == 'isreadonly':
            pass
        elif pt.tag == 'isoptional':
            pt_is_opt = ' ' + pt.tag  # bit of a hack - spacing is not ideal
        elif pt.tag == 'BlobDataType':
            pt_type = pt.tag
            blob_attribs = pt.attrib  # Not used - maybe later?
        elif pt.tag.find('DataType') > 0:     # Assume every DT has this token
            pt_type = pt.tag
        else:
            print('Error: unknown property type tag: ' + pt.tag)  # Should never get here - ever
    property_dict[pt_name] = pt_type + pt_is_opt


# Render an entity type as a node in main, dot cluster
def do_render_node(et_name: str, property_dict: dict) -> None:
    global et_names_set
    if et_name in et_names_set:      # Don't render it if it's already rendered
        return
    global dot
    maxkey_Len = 0
    for key in property_dict.keys():
        if len(key) > maxkey_Len:
            maxkey_Len = len(key)
    prop_Label = '|{'
    for key, value in property_dict.items():
        prop_Label = prop_Label + key + (maxkey_Len - len(key)) * '\ ' + ' : ' + value + '\l|'
    prop_Label = prop_Label.rstrip(prop_Label[-1]) + '}'
    # Write the node in this SA's digraph, all et's in the model file are in the parent cluster
    dot.node(et_name, area='5', shape='record', label=et_name + prop_Label)
    et_names_set.add(et_name)     # Add it to the rendered node set


# Caches ET's that are not in this SA with their own SA
def do_cache_et_in_sa(et_name: str) -> None:
    global cluster_node_set_dict
    global sa_id
    split_name = et_name.split('/')  # Get SA of the src ET
    if split_name[0] != et_name and split_name[0] != sa_id:  # not in the main SA
        # Cache the et name in the set of et's mapped to this SA
        try:
            sa_ets = cluster_node_set_dict[split_name[0]]  # set of all nodes in this SA
        except KeyError as ke:                             # Nothing there yet
            sa_ets = set()                                 # New set
        sa_ets.add(et_name)
        cluster_node_set_dict[split_name[0]] = sa_ets


# Deal with the ET
def do_entity_type(the_et: ET.Element):
    global sa_id
    global reln_dict                         # Needed for any supertype reln
    global cluster_node_set_dict
    property_dict = dict()  # dictionary the maps the PT name->type+isopt
    et_superType = None
    for et in the_et:
        if et.tag == 'name':
            et_name = et.text
        elif et.tag == 'username':
            et_username = et.text
        elif et.tag == 'userdescription':
            et_userdsc = et.text
        elif et.tag == 'supertype':
            # Cache this edge - just another relationship
            et_super_type = et.text
            reln_tuple = (et_name, et_super_type, '', '')
            reln_dict['supertype'] = reln_tuple
            # If the supertype is in another SA cache it - else it can be emitted as a node in this graph
            do_cache_et_in_sa(et_super_type)
            # else - nop, the supertype is in this SA it'll get rendered as an ET
        elif et.tag == 'propertytype':
            do_property_type(et, property_dict)
        elif et.tag == 'isabstract':
            pass
        elif et.tag == 'maybeusedasforwardreference':
            pass
        else:
            print('Error: unrecognized ET tag: ' + et.tag)
    # Render the ET, its PT's in this SA
    do_render_node(et_name, property_dict)


# Run this once we've got all the sa's and the dot object created. Iterate thru the SA dictionary of
# sets of nodes and render them in with
def do_render_sub_graphs() -> None:
    global dot
    global cluster_node_set_dict
    global node_shape
    global node_fontname

    for cluster_name, node_set in cluster_node_set_dict.items():
        a_lab = '    label = "' + cluster_name + '";\n'
        with dot.subgraph(name='cluster_' +  cluster_name, comment='Subject area: ' + cluster_name,
                                 node_attr={'shape': node_shape, 'fontname': node_fontname},
                                 graph_attr={'compound': 'true'}, body=[a_lab]
                                 ) as a_subgraph:
            isEmpty = (len(node_set) == 0)
            if not isEmpty:
                for node in node_set:
                    name = node.split('/')
                    a_subgraph.node(node, area='5', shape=node_shape, label=name[1])
            else:
                print('Error: set of nodes for SA: ' + cluster_name + ' is empty')


# Cache RT's and render them at the end as edges
def do_relationship_type(the_rt: ET.Element) -> None:
    global dot
    global reln_dict
    global sa_id
    global cluster_node_set_dict
    reln_name = None
    source_name = ''
    dest_name = ''
    for rt in the_rt:
        if rt.tag == 'name':
            reln_name = rt.text
        elif rt.tag == 'username':
            reln_username = rt.text
        elif rt.tag == 'userdescription':
            reln_userdesc = rt.text
        elif rt.tag == 'src':
            for src in rt:
                if src.tag == 'e':
                    # Possibly cache this if it's not in this SA
                    source_name = src.text
                    do_cache_et_in_sa(source_name)
                elif src.tag == 'MultiplicityValue':
                    src_mult = src.text
                elif src.tag == 'supertype':
                    pass
                elif src.tag == 'propertytype':
                    pass
                else:
                    print('Error: unexpected RT source tag: ' + src.tag)
        elif rt.tag == 'dest':
            for dest in rt:
                if dest.tag == 'e':
                    # Possibly cache it
                    dest_name = dest.text
                    do_cache_et_in_sa(dest_name)
                    # else: Source et is in our SA - so it'll be rendered
                elif dest.tag == 'MultiplicityValue':
                    dest_mult = dest.text
                elif dest.tag == 'supertype':
                    pass
                elif dest.tag == 'propertytype':
                    pass
                else:
                    print('Error: unexpected RT dest tag: ' + dest.tag)
    if reln_name is not None:
        if source_name == '':
            source_name = '"Object"'
        if dest_name == '':
            dest_name = '"Object"'
        reln_tuple = (source_name, dest_name, src_mult, dest_mult)
        print(reln_tuple)
        reln_dict[reln_name] = reln_tuple
    else:
        print('Error: parsing relationship type - name missing')


# Renders all the relatonships as edges - called last
def do_render_edges() -> None:
    global reln_dict
    global dot
    global edge_select
    for reln_name, reln_tuple in reln_dict.items():
        (src_name, dest_name, src_mult, dest_mult) = reln_tuple
        if reln_name == 'supertype':
            dot.edge(dest_name + '::s', src_name + '::n', label=reln_name, constraint='true', dir='back')
        else:
            head_con = '_'
            tail_con = '_'
            dot.edge(src_name, dest_name, label=reln_name, labelfloat='false', constraint='true', dir='both',
                     headlabel=dest_mult, minlen='2.0', headport=head_con, taillabel=src_mult,
                     arrowsize='.4', tailport=tail_con, labelfontsize='10')


# Parse the SA and create the dot file representation
def do_subject_area(the_sa: ET.Element) -> None:
    do_sa_id(the_sa)
    for x in the_sa:
        if x.tag == 'name':
            sa_name = x.text
        elif x.tag == 'username':
            sa_username = x.text
        elif x.tag == 'userdescription':
            sa_userdescription = 'x.text'
        elif x.tag == 'dependson':
            do_depends_on(x)
        elif x.tag == 'entitytype':
            do_entity_type(x)
        elif x.tag == 'relationshiptype':
            do_relationship_type(x)
        else:
            print('Unrecognized subject area tag: ' + x.tag)

# Main

path = None
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Parse a model.xml file and render it using graphviz as a nice '
                                                 'ER-like diagram.',
                                     epilog="Service and support mailto:adrianblakey@gmail.com")
    parser.add_argument('path', help='render a model.xml file using graphviz', type=str, nargs='?', default='model.xml')
    args = parser.parse_args()
    path = args.path

if not os.path.isfile(path):
    print('Error: the input file name: ' + path + ' is not a file.')
    exit(5)

the_tree = ET.parse(path)
the_sa = the_tree.getroot()

if (the_sa.tag) != 'subjectarea':
    print("Error: Expecting the main tag to be subjectarea, found: " + the_sa.tag)
    exit(4)

do_subject_area(the_sa)
do_render_sub_graphs()
do_render_edges()

print(dot.source)  # doctest: +NORMALIZE_WHITESPACE +NO_EXEs

dot.render('output/diagram.gv').replace('\\', '/')

dot.render('output/diagram.gv', view=True)  # doctest: +SKIP

exit(0);

