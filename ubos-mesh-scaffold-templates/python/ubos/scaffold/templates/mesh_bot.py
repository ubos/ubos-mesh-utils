#!/usr/bin/python
#
# Copyright (C) 2017 and later, Indie Computing Corp. All rights reserved. License: see package.
#

from ubos.scaffold.template import AbstractTemplate
import ubos.scaffold.utils


class MeshBot( AbstractTemplate ):
    """
    Template for UBOS Mesh Bots
    """
    def generate( self, pars, directory ):
        ret = super().generate( pars, directory )

        vlNameAsPath     = pars['subname'].replace( '.', '/' )
        vlNameAsCssClass = pars['subname'].replace( '.', '-' )

        ubos.scaffold.utils.ensureDirectories(
                directory + "/" + pars['subname'] + "/src/main/java/"      + vlNameAsPath,
                directory + "/" + pars['subname'] + "/src/main/resources/" + vlNameAsPath )

        ubos.utils.saveFile( directory + "/" + pars['subname'] + "/src/main/java/" + vlNameAsPath + "/ModuleInit.java", f"""\
//
//
//

package { pars['subname'] };

import net.ubos.daemon.Daemon;
import org.diet4j.core.Module;
import org.diet4j.core.ModuleActivationException;
import org.diet4j.core.ModuleDeactivationException;

/**
 * Module initializer class.
 */
public class ModuleInit
{{
    /**
     * Diet4j module activation.
     *
     * @param thisModule the Module being activated
     * @throws ModuleActivationException thrown if Module activation failed
     */
    public static void moduleActivate(
            Module thisModule )
        throws
            ModuleActivationException
    {{
        if( theBot == null ) {{
            theBot = new MyBot();
        }}
        Daemon.registerBot( theBot );
    }}

    /**
     * Diet4j module deactivation.
     *
     * @param thisModule the Module being deactivated
     * @throws ModuleDeactivationException thrown if module deactivation failed
     */
    public static void moduleDectivate(
            Module thisModule )
        throws
            ModuleDeactivationException
    {{
        Daemon.unregisterBot( theBot );
    }}

   /**
     * My Bot.
     */
    protected static MyBot theBot;
}}
""".encode())

        ubos.utils.saveFile( directory + "/" + pars['subname'] + "/src/main/java/" + vlNameAsPath + "/MyBot.java", f"""\
//
//
//

package { pars['subname'] };

import net.ubos.bot.Bot;
import net.ubos.meshbase.transaction.Change;
import net.ubos.meshbase.transaction.ChangeSet;

/**
 *
 */
public class MyBot
    implements
        Bot
{{
    /**
     * {{@inheritDoc}}
     */
    @Override
    public void notifyChanges(
            ChangeSet changes )
    {{
        // main code

        // for( Change change : changes ) {{
        //     doSomething( change );
        // }}
    }}

    /**
     * The Attribute name indicating who did this.
     */
    public static final String CREATOR = "bot-created";

    /**
     * The Attribute value for me as creator.
     */
    public static final String CREATOR_NAME = MyBot.class.getName();
}}
""".encode())


        ubos.utils.saveFile( directory + "/" + pars['subname'] + "/build.gradle", f"""\
description = '{ pars['subname'] }'

dependencies {{
    api "net.ubos:net.ubos.kernel:+"
    // api "net.ubos:net.ubos.model.Structure:+"
    compileOnly "org.diet4j:diet4j-core:${{diet4j_version}}"
}}
""".encode())


        ubos.utils.saveFile( directory + "/build.gradle", f"""\
description = '{ pars['name'] }'

dependencies {{
    api project(':{ pars['name'] }:{ pars['subname'] }')
}}
""".encode())

        return ret


    def pars( self ) :
        ret = super().pars()

        ret['name'] = {
            'index'       : 10,
            'description' : """\
Name of the Bot package (e.g. ubos-mesh-bot-southpole)
"""
        }

        ret['subname'] = {
            'index'       : 20,
            'description' : """\
Name of the Bot (e.g. net.ubos.bot.southpole)
"""
        }

        ret['groupid'] = {
            'index'       : 30,
            'description' : """\
The Maven Group ID for the Bot (e.g. net.ubos.bot)
"""
        }

        return ret


    def pkgbuildContentVars( self, pars, directory ):
        """
        Obtain the bash variables in the PKGBUILD file. This returns a hash,
        so it is easier for subclasses to incrementally modify.

        pars: the parameters to use
        directory: the output directory
        return: the content
        """

        ret = {
            'pkgname'    : '$(basename $(pwd))',
            'pkgver'     : '$(cat ../PKGVER)',
            'pkgrel'     : '1',
            'pkgdesc'    : '"' + pars['description'] + '"',
            'developer'  : '"' + pars['developer']   + '"',
            'url'        : '"' + pars['url']         + '"',
            'maintainer' : '"' + pars['developer']   + '"',
            'arch'       : '("any")',
            'license'    : '("AGPL3")',
            'depends'    : """\
(
    # Insert your UBOS package dependencies here as a bash array, like this:
    'ubos-mesh-model-library'
)\
""",
            'makedepends' : """\
(
    'gradle'
)\
""",
            'options'  : "('!strip')",
            '_jv'      : '$(cat ../JV)',
            '_m2repo'  : "${GRADLE_M2_HOME:-${HOME}/.m2/repository}",
            '_groupId' : '"' + pars['groupid'] + '"'
        }

        return ret


    def pkgbuildContentBuild( self, pars, directory ):
        return """\
    cd ${startdir}

    if [[ "${GRADLE_M2_HOME}" != '' ]]; then
        export M2_HOME=${_m2repo}
    fi
    JAVA_HOME=/usr/lib/jvm/java-${_jv}-openjdk gradle jar
"""


    def pkgbuildContentOther( self, pars, directory ):

        return """
installJar() {
    local name=$1
    install -m644 -D ${_m2repo}/${_groupId//.//}/${name}/${pkgver}/${name}-${pkgver}.{jar,pom} -t ${pkgdir}/ubos/lib/java/${_groupId//.//}/${name}/${pkgver}/
}
"""


    def gitIgnoreContent( self, pars, directory ):

        return f"""\
{ pars['name'] }-*.pkg*
.gradle/
"""


    def manifestContent( self, pars, directory ):
        return f"""\
{{
    "type"  : "accessory",

    "accessoryinfo" : {{
        "appids" : [
            "ubos-mesh-underbars-mysql"
        ]
    }}
}}
"""


    def pkgbuildContentPackage( self, pars, directory ):
        """
        Obtain the content of the package method in the PKGBUILD file.

        pars: the parameters to use
        directory: the output directory
        return: the content
        """

        return f"""\
    # Manifest
    install -D -m0644 ${{startdir}}/ubos-manifest.json ${{pkgdir}}/ubos/lib/ubos/manifests/${{pkgname}}.json

    # Code
    installJar '{ pars['subname'] }'
    installJar '{ pars['name']   }'\
"""


def create() :
    """
    Factory function
    """
    return MeshBot()


def help() :
    return 'UBOS Mesh Bot'

