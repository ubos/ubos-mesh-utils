#!/usr/bin/python
#
# Copyright (C) 2017 and later, Indie Computing Corp. All rights reserved. License: see package.
#

from ubos.scaffold.template import AbstractAppOrAccessoryTemplate
import ubos.scaffold.utils


class MeshUnderbarsViewlet( AbstractAppOrAccessoryTemplate ):
    """
    Template for UBOS Mesh Viewlets that have been created for Undertow and Handlebars
    """

    def generate( self, pars, directory ):
        ret = super().generate( pars, directory )

        vlNameAsPath     = pars['subname'].replace( '.', '/' )
        vlNameAsCssClass = pars['subname'].replace( '.', '-' )

        ubos.scaffold.utils.ensureDirectories(
                directory + "/assets/" + pars['subname'],
                directory + "/viewlets/" + pars['subname'],
                directory + "/" + pars['subname'] + "/src/main/java/"      + vlNameAsPath,
                directory + "/" + pars['subname'] + "/src/main/resources/" + vlNameAsPath )

        ubos.utils.saveFile( directory + "/assets/" + pars['subname'] + "/" + pars['subname'] + ".css", f"""\
div.{ vlNameAsCssClass }.vl {{
}}

div.{ vlNameAsCssClass }.vl-contained {{
    padding: 5px;
}}
div.{ vlNameAsCssClass }.vl-contained p {{
    margin: 0;
    padding: 0;
}}
div.{ vlNameAsCssClass }.vl-contained span.h {{
    font-weight: var( --font-weight-bold );
}}
""".encode() )

        ubos.utils.saveFile( directory + "/" + pars['subname'] + "/src/main/java/" + vlNameAsPath + "/ModuleInit.java", f"""\
//
//
//

package { pars['subname'] };

import java.io.IOException;
import net.ubos.underbars.Underbars;
import net.ubos.underbars.resources.PackageAwareResourceManager;
import net.ubos.underbars.vl.DefaultHtmlHandlebarsViewletFactoryChoice;
import net.ubos.util.ResourceHelper;
import net.ubos.vl.MeshObjectsToView;
import net.ubos.vl.ViewletDetail;
import net.ubos.vl.ViewletDimensionality;
import net.ubos.vl.ViewletFactoryChoice;
import net.ubos.web.vl.WebViewletPlacement;
import org.diet4j.core.Module;
import org.diet4j.core.ModuleActivationException;

/**
 * Module initializer class.
 */
public class ModuleInit
{{
    /**
     * Name of this package.
     */
    public static final String UBOS_PACKAGE_NAME = "{ pars['name'] }";

    /**
     * Name of the Viewlet.
     */
    public static final String VL_NAME = "{ pars['subname'] }";

    /**
     * Diet4j module activation.
     *
     * @param thisModule the Module being activated
     * @throws ModuleActivationException thrown if Module activation failed
     */
    public static void moduleActivate(
            Module thisModule )
        throws
            ModuleActivationException
    {{
        try {{
            PackageAwareResourceManager resourceManager = PackageAwareResourceManager.createEmpty( UBOS_PACKAGE_NAME );

            Underbars.registerViewlet(
                    (MeshObjectsToView toView) -> {{
                            return null;

                            // if( toView.getSubject().isBlessedBy( SomeSubjectArea.SOMEENTITYTYPE )) {{
                            //     return DefaultHtmlHandlebarsViewletFactoryChoice.createSeveral(
                            //             VL_NAME,
                            //             resourceManager,
                            //             ResourceHelper.getInstance( VL_NAME + ".VL", ModuleInit.class.getClassLoader() ),
                            //             WebViewletPlacement.ROOT,    ViewletDimensionality.DIM_2D, ViewletDetail.NORMAL,   ViewletFactoryChoice.GOOD_MATCH_QUALITY,
                            //             WebViewletPlacement.INLINED, ViewletDimensionality.DIM_2D, ViewletDetail.OVERVIEW, ViewletFactoryChoice.AVERAGE_MATCH_QUALITY );

                            // }} else {{
                            //     return null;
                            // }}
                    }} );


            Underbars.registerAssets( resourceManager.getAssetMap() );

            resourceManager.checkResources();

        }} catch( IOException ex ) {{
            throw new ModuleActivationException( thisModule.getModuleMeta(), ex );
        }}
    }}
}}
""".encode())

        ubos.utils.saveFile( directory + "/" + pars['subname'] + "/src/main/resources/" + vlNameAsPath + "/VL.properties", f"""\
#
#
#

UserVisibleName=Unnamed
""".encode())

        ubos.utils.saveFile( directory + "/" + pars['subname'] + "/build.gradle", f"""\
description = '{ pars['subname'] }'

dependencies {{
    // api "net.ubos:net.ubos.model.Structure:+"
    api "net.ubos:net.ubos.underbars:+"
    compileOnly "org.diet4j:diet4j-core:${{diet4j_version}}"
}}
""".encode())


        ubos.utils.saveFile( directory + "/build.gradle", f"""\
description = '{ pars['name'] }'

dependencies {{
    api project(':{ pars['name'] }:{ pars['subname'] }')
}}
""".encode())

        ubos.utils.saveFile( directory + "/viewlets/" + pars['subname'] + "/html_body_contained.hbs", f"""\
{{{{  html:link rel="stylesheet" href="/s/{ pars['subname'] }/{ pars['subname'] }.css" }}}}

{{{{# vl:vl }}}}

<p>
 <span class="h">{{{{ mesh:meshObject meshObject=subject }}}}</span>
</p>

{{{{/ vl:vl }}}}
""".encode())

        ubos.utils.saveFile( directory + "/viewlets/" + pars['subname'] + "/html_body_content.hbs", f"""\
{{{{  html:link rel="stylesheet" href="/s/{ pars['subname'] }/{ pars['subname'] }.css" }}}}

{{{{# vl:vl }}}}

<h1>{{{{ mesh:meshObject meshObject=subject }}}}</h1>

{{{{/ vl:vl }}}}
""".encode())

        ubos.utils.saveFile( directory + "/viewlets/" + pars['subname'] + "/html_head_title.hbs", f"""\
{ pars['subname'] }
""".encode())

        return ret


    def pars( self ) :
        ret = super().pars()

        ret['name'] = {
            'index'       : 10,
            'description' : """\
Name of the Viewlet package (e.g. ubos-mesh-underbars-vl-northpole)
"""
        }

        ret['subname'] = {
            'index'       : 20,
            'description' : """\
Name of the Viewlet (e.g. net.ubos.underbars.vl.northpole)
"""
        }

        ret['groupid'] = {
            'index'       : 30,
            'description' : """\
The Maven Group ID for the Viewlet (e.g. net.ubos.underbars)
"""
        }

        return ret


    def pkgbuildContentVars( self, pars, directory ):
        """
        Obtain the bash variables in the PKGBUILD file. This returns a hash,
        so it is easier for subclasses to incrementally modify.

        pars: the parameters to use
        directory: the output directory
        return: the content
        """

        ret = {
            'pkgname'    : '$(basename $(pwd))',
            'pkgver'     : '$(cat ../PKGVER)',
            'pkgrel'     : '1',
            'pkgdesc'    : '"' + pars['description'] + '"',
            'developer'  : '"' + pars['developer']   + '"',
            'url'        : '"' + pars['url']         + '"',
            'maintainer' : '"' + pars['developer']   + '"',
            'arch'       : '("any")',
            'license'    : '("AGPL3")',
            'depends'    : """\
(
    # Insert your UBOS package dependencies here as a bash array, like this:
    'ubos-mesh-model-library'
    'ubos-mesh-underbars'
)\
""",
            'makedepends' : """\
(
    'gradle'
)\
""",
            'options'  : "('!strip')",
            '_jv'      : '$(cat ../JV)',
            '_m2repo'  : "${GRADLE_M2_HOME:-${HOME}/.m2/repository}",
            '_groupId' : '"' + pars['groupid'] + '"'
        }

        return ret


    def pkgbuildContentBuild( self, pars, directory ):
        return """\
    cd ${startdir}

    if [[ "${GRADLE_M2_HOME}" != '' ]]; then
        export M2_HOME=${_m2repo}
    fi
    JAVA_HOME=/usr/lib/jvm/java-${_jv}-openjdk gradle jar
"""


    def pkgbuildContentOther( self, pars, directory ):

        return """
installJar() {
    local name=$1
    install -m644 -D ${_m2repo}/${_groupId//.//}/${name}/${pkgver}/${name}-${pkgver}.{jar,pom} -t ${pkgdir}/ubos/lib/java/${_groupId//.//}/${name}/${pkgver}/
}
"""


    def gitIgnoreContent( self, pars, directory ):

        return f"""\
{ pars['name'] }-*.pkg*
.gradle/
"""


    def manifestContent( self, pars, directory ):
        return f"""\
{{
    "type"  : "accessory",

    "accessoryinfo" : {{
        "appids" : [
            "ubos-mesh-underbars-mysql"
        ]
    }}
}}
"""

    def pkgbuildContentPackage( self, pars, directory ):
        """
        Obtain the content of the package method in the PKGBUILD file.

        pars: the parameters to use
        directory: the output directory
        return: the content
        """

        return f"""\
    # Manifest
    install -D -m0644 ${{startdir}}/ubos-manifest.json ${{pkgdir}}/ubos/lib/ubos/manifests/${{pkgname}}.json

    # Icons
    # install -D -m0644 ${{startdir}}/appicons/{{72x72,144x144}}.png -t ${{pkgdir}}/ubos/http/_appicons/${{pkgname}}/

    # Assets and viewlets
    mkdir -p ${{pkgdir}}/ubos/share/${{pkgname}}
    cp -a ${{startdir}}/{{assets,viewlets}} ${{pkgdir}}/ubos/share/${{pkgname}}/

    # Code
    installJar '{ pars['subname'] }'
    installJar '{ pars['name']   }'\
"""


def create() :
    """
    Factory function
    """
    return MeshUnderbarsViewlet()


def help() :
    return 'UBOS Mesh Viewlet using Undertow and Handlebars'

