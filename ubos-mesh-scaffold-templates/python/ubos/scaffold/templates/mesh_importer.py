#!/usr/bin/python
#
# Copyright (C) 2017 and later, Indie Computing Corp. All rights reserved. License: see package.
#

from ubos.scaffold.template import AbstractTemplate
import ubos.scaffold.utils


class MeshImporter( AbstractTemplate ):
    """
    Template for UBOS Mesh Importers
    """
    def generate( self, pars, directory ):
        ret = super().generate( pars, directory )

        subNameAsPath = pars['subname'].replace( '.', '/' )

        className = pars['subname']
        if '.' in className:
            className = className[ className.rfind('.') + 1 : ]
        className = className.capitalize() + 'Importer'

        manifestContent = self.manifestContent( pars, directory );
        if manifestContent:
            ubos.utils.saveFile( directory + "/ubos-manifest.json", manifestContent.encode(), 0o644 )

        ubos.scaffold.utils.ensureDirectories(
                directory + "/" + pars['subname'] + '/src/main/java/' + subNameAsPath )

        ubos.utils.saveFile( directory + '/' + pars['subname'] + '/src/main/java/' + subNameAsPath + '/ModuleInit.java', f"""\
//
//
//

package { pars['subname'] };

import net.ubos.importer.Importer;
import net.ubos.importer.ImporterException;
import net.ubos.meshbase.MeshBase;
import org.diet4j.core.Module;
import org.diet4j.core.ModuleActivationException;

/**
 * Activate this Module and return an Importer
 */
public class ModuleInit
{{
    /**
     * Diet4j module activation.
     *
     * @param thisModule the Module being activated
     * @return the Importer instances
     * @throws ModuleActivationException thrown if module activation failed
     */
    public static Importer [] moduleActivate(
            Module thisModule )
        throws
            ModuleActivationException
    {{
        return new Importer[] {{
            new { className }()
        }};
    }}
}}

""".encode())

        ubos.utils.saveFile( directory + '/' + pars['subname'] + '/src/main/java/' + subNameAsPath + '/' + className + '.java', f"""\
//
//
//

package { pars['subname'] };

import net.ubos.importer.Importer;

/**
 * The Importer.
 */
public class { className }
    implements
        Importer
{{
}}

""".encode())

        ubos.utils.saveFile( directory + '/' + pars['subname'] + '/build.gradle', f"""\
description = '{ pars['subname'] }'

dependencies {{
    api "net.ubos:net.ubos.kernel:+"
    // api "net.ubos:net.ubos.model.Structure:+"
    compileOnly "org.diet4j:diet4j-core:${{diet4j_version}}"
}}
""".encode())


        ubos.utils.saveFile( directory + "/build.gradle", f"""\
description = '{ pars['name'] }'

dependencies {{
    api project(':{ pars['name'] }:{ pars['subname'] }')
}}
""".encode())

        return ret


    def pars( self ) :
        ret = super().pars()

        ret['name'] = {
            'index'       : 10,
            'description' : """\
Name of the importer package (e.g. ubos-mesh-importer-eastpole)
"""
        }

        ret['subname'] = {
            'index'       : 20,
            'description' : """\
Name of the importer (e.g. net.ubos.importer.eastpole)
"""
        }

        ret['groupid'] = {
            'index'       : 30,
            'description' : """\
The Maven Group ID for the importer (e.g. net.ubos.eastpole)
"""
        }

        return ret


    def pkgbuildContentVars( self, pars, directory ):
        """
        Obtain the bash variables in the PKGBUILD file. This returns a hash,
        so it is easier for subclasses to incrementally modify.

        pars: the parameters to use
        directory: the output directory
        return: the content
        """

        ret = {
            'pkgname'    : '$(basename $(pwd))',
            'pkgver'     : '$(cat ../PKGVER)',
            'pkgrel'     : '1',
            'pkgdesc'    : '"' + pars['description'] + '"',
            'developer'  : '"' + pars['developer']   + '"',
            'url'        : '"' + pars['url']         + '"',
            'maintainer' : '"' + pars['developer']   + '"',
            'arch'       : '("any")',
            'license'    : '("AGPL3")',
            'depends'    : """\
(
    # Insert your UBOS package dependencies here as a bash array, like this:
    'ubos-mesh-kernel'
    'ubos-mesh-model-library'
)\
""",
            'makedepends' : """\
(
    'gradle'
)\
""",
            'options'  : "('!strip')",
            '_jv'      : '$(cat ../JV)',
            '_m2repo'  : "${GRADLE_M2_HOME:-${HOME}/.m2/repository}",
            '_groupId' : '"' + pars['groupid'] + '"'
        }

        return ret


    def pkgbuildContentBuild( self, pars, directory ):
        return """\
    cd ${startdir}

    if [[ "${GRADLE_M2_HOME}" != '' ]]; then
        export M2_HOME=${_m2repo}
    fi
    JAVA_HOME=/usr/lib/jvm/java-${_jv}-openjdk gradle jar
"""


    def pkgbuildContentOther( self, pars, directory ):

        return """
installJar() {
    local name=$1
    install -m644 -D ${_m2repo}/${_groupId//.//}/${name}/${pkgver}/${name}-${pkgver}.{jar,pom} -t ${pkgdir}/ubos/lib/java/${_groupId//.//}/${name}/${pkgver}/
}
"""


    def gitIgnoreContent( self, pars, directory ):

        return f"""\
{ pars['name'] }-*.pkg*
.gradle/
"""

    def pkgbuildContentPackage( self, pars, directory ):
        """
        Obtain the content of the package method in the PKGBUILD file.

        pars: the parameters to use
        directory: the output directory
        return: the content
        """

        return f"""\
    # Code
    installJar '{ pars['subname'] }'
    installJar '{ pars['name']   }'\
"""


def create() :
    """
    Factory function
    """
    return MeshImporter()


def help() :
    return 'UBOS Mesh Importer'

