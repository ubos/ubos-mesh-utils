#!/usr/bin/python
#
# Copyright (C) 2017 and later, Indie Computing Corp. All rights reserved. License: see package.
#

from ubos.scaffold.template import AbstractTemplate
import ubos.scaffold.utils


class MeshModel( AbstractTemplate ):
    """
    Template for UBOS Mesh Models
    """
    def generate( self, pars, directory ):
        ret = super().generate( pars, directory )

        ubos.scaffold.utils.ensureDirectories(
                directory + "/" + pars['subname'] + "/src/main/models/" )

        ubos.utils.saveFile( directory + "/" + pars['subname'] + "/src/main/models/model.xml", f"""\
<?xml version="1.0" encoding="UTF-8"?>

<subjectarea ID="{ pars['subname'] }">
    <name>{ pars['subname'] }</name>
<!--
    <username>User-friendly name</username>
    <userdescription>User-friendly description</userdescription>
-->

<!-- Uncomment to declare dependencies on other SubjectAreas
    <dependson>
        <subjectareareference>
            <name>net.ubos.model.Structure</name>
            <name>net.ubos.model.ThreadedNotes</name>
        </subjectareareference>
    </dependson>
-->

<!-- Example EntityType with PropertyType declaration
    <entitytype>
        <name>SomeEntityType</name>
        <username>User-friendly name</username>
        <userdescription>User-friendly description</userdescription>

        <supertype>net.ubos.model.Structure/Collection</supertype>

        <propertytype>
            <name>SomePropertType</name>
            <username>User-friendly name</username>
            <userdescription>User-friendly description</userdescription>
            <StringDataType/>
            <isoptional/>
        </propertytype>
    </entitytype>
-->

<!-- Example RelationshipType with PropertyType declaration
    <relationshiptype>
        <name>SomeEntityType_RelatesTo_SomeOtherEntityType</name>
        <username>User-friendly name</username>
        <userdescription>User-friendly description</userdescription>
        <src>
            <e>SomeEntityType</e>
            <MultiplicityValue>0:1</MultiplicityValue>

            <propertytype>
                <name>SomePropertType</name>
                <username>User-friendly name</username>
                <userdescription>User-friendly description</userdescription>
                <IntegerDataType/>
                <isoptional/>
            </propertytype>

        </src>
        <dest>
            <e>SomeOtherEntityType</e>
            <MultiplicityValue>0:n</MultiplicityValue>
        </dest>
    </relationshiptype>
-->
</subjectarea>

""".encode())

        ubos.utils.saveFile( directory + "/" + pars['subname'] + "/build.gradle", f"""\
description = '{ pars['subname'] }'

dependencies {{
    api "net.ubos:net.ubos.kernel:+"
    // api "net.ubos:net.ubos.model.Structure:+"
    compileOnly "org.diet4j:diet4j-core:${{diet4j_version}}"
}}
""".encode())


        ubos.utils.saveFile( directory + "/build.gradle", f"""\
description = '{ pars['name'] }'

dependencies {{
    api project(':{ pars['name'] }:{ pars['subname'] }')
}}
""".encode())

        return ret



    def pars( self ) :
        ret = super().pars()

        ret['name'] = {
            'index'       : 10,
            'description' : """\
Name of the model package (e.g. ubos-mesh-model-westpole)
"""
        }

        ret['subname'] = {
            'index'       : 20,
            'description' : """\
Name of the model (e.g. net.ubos.model.westpole)
"""
        }

        ret['groupid'] = {
            'index'       : 30,
            'description' : """\
The Maven Group ID for the model (e.g. net.ubos.westpole)
"""
        }

        return ret


    def pkgbuildContentVars( self, pars, directory ):
        """
        Obtain the bash variables in the PKGBUILD file. This returns a hash,
        so it is easier for subclasses to incrementally modify.

        pars: the parameters to use
        directory: the output directory
        return: the content
        """

        ret = {
            'pkgname'    : '$(basename $(pwd))',
            'pkgver'     : '$(cat ../PKGVER)',
            'pkgrel'     : '1',
            'pkgdesc'    : '"' + pars['description'] + '"',
            'developer'  : '"' + pars['developer']   + '"',
            'url'        : '"' + pars['url']         + '"',
            'maintainer' : '"' + pars['developer']   + '"',
            'arch'       : '("any")',
            'license'    : '("AGPL3")',
            'depends'    : """\
(
    # Insert your UBOS package dependencies here as a bash array, like this:
    'ubos-mesh-model-library'
)\
""",
            'makedepends' : """\
(
    'gradle'
)\
""",
            'options'  : "('!strip')",
            '_jv'      : '$(cat ../JV)',
            '_m2repo'  : "${GRADLE_M2_HOME:-${HOME}/.m2/repository}",
            '_groupId' : '"' + pars['groupid'] + '"'
        }

        return ret


    def pkgbuildContentBuild( self, pars, directory ):
        return """\
    cd ${startdir}

    if [[ "${GRADLE_M2_HOME}" != '' ]]; then
        export M2_HOME=${_m2repo}
    fi
    JAVA_HOME=/usr/lib/jvm/java-${_jv}-openjdk gradle jar
"""


    def pkgbuildContentOther( self, pars, directory ):

        return """
installJar() {
    local name=$1
    install -m644 -D ${_m2repo}/${_groupId//.//}/${name}/${pkgver}/${name}-${pkgver}.{jar,pom} -t ${pkgdir}/ubos/lib/java/${_groupId//.//}/${name}/${pkgver}/
}
"""


    def gitIgnoreContent( self, pars, directory ):

        return f"""\
{ pars['name'] }-*.pkg*
.gradle/
"""


    def manifestContent( self, pars, directory ):
        return f"""\
{{
    "type"  : "accessory",

    "accessoryinfo" : {{
        "appids" : [
            "ubos-mesh-underbars-mysql"
        ]
    }}
}}
"""


    def pkgbuildContentPackage( self, pars, directory ):
        """
        Obtain the content of the package method in the PKGBUILD file.

        pars: the parameters to use
        directory: the output directory
        return: the content
        """

        return f"""\
    # Manifest
    install -D -m0644 ${{startdir}}/ubos-manifest.json ${{pkgdir}}/ubos/lib/ubos/manifests/${{pkgname}}.json

    # Code
    installJar '{ pars['subname'] }'
    installJar '{ pars['name']   }'\
"""


def create() :
    """
    Factory function
    """
    return MeshModel()


def help() :
    return 'UBOS Mesh Model'

