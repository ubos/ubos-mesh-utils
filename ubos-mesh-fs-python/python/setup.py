#!/usr/bin/python
#
# Setup the package.
#
# Copyright (C) Johannes Ernst. All rights reserved. License: see package.
#

from pathlib import Path
from setuptools import setup
import ubos.mesh.fs

setup(name='ubos-mesh-fs',
      version=Path('../PKGVER').read_text().strip(),
      author='Johannes Ernst',
      license='AGPLv3',
      description='Python interface to UBOS Data Mesh FS',
      url='https://gitlab.com/ubos/ubos-mesh-fs-python',
      packages=[
          'ubos.mesh.fs',
          'ubos.mesh.fs.commands'
      ],
      zip_safe=False)
