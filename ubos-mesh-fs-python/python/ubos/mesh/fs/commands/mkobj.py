#!/usr/bin/python
#
# Create a MeshObject and optionally bless it.
#
# Copyright (C) Johannes Ernst. All rights reserved. License: see package.
#

import argparse
import ubos.logging
import ubos.utils

from ubos.mesh.fs.MeshBase import MeshBase
from ubos.mesh.fs.MeshObject import MeshObject

def run( args ) :
    """
    Run this command.

    args: parsed command-line arguments
    """

    ( mb, meshObjectId ) = MeshBase.findMeshBase( args.meshobject )
    meshObject           = mb.findMeshObject( meshObjectId )

    if meshObject is not None:
        ubos.logging.fatal( 'MeshObject exists already:', meshObjectId, 'in MeshBase with root at:', mb.getRootDir() )

    meshObject = mb.createMeshObject( meshObjectId, args.entitytype );
    meshObject.save();

    ubos.logging.info( 'Created:', meshObject.getPath() )


def addSubParser( parentParser, cmdName ) :
    """
    Enable this command to add its own command-line options
    parentParser: the parent argparse parser
    cmdName: name of this command
    """
    parser = parentParser.add_parser( cmdName,    help='Bless an already-existing MeshObject.' )
    parser.add_argument( 'meshobject',            help='The MeshObject')
    parser.add_argument( 'entitytype', nargs='*', help='The EntityType(s) to bless with' )
