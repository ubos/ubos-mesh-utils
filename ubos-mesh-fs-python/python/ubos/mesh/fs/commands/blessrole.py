#!/usr/bin/python
#
# Bless a role of two already-related MeshObjects.
#
# Copyright (C) Johannes Ernst. All rights reserved. License: see package.
#

import argparse
import ubos.logging
import ubos.utils

from ubos.mesh.fs.MeshBase import MeshBase
from ubos.mesh.fs.MeshObject import MeshObject

def run( args ) :
    """
    Run this command.

    args: parsed command-line arguments
    """

    ( mb1, meshObjectId ) = MeshBase.findMeshBase( args.meshobject )
    meshObject            = mb1.findMeshObject( meshObjectId )

    ( mb2, neighborId ) = MeshBase.findMeshBase( args.neighbor )
    neighbor            = mb2.findMeshObject( neighborId )

    if mb1 != mb2 :
        ubos.logging.fatal( 'The provided MeshObjects belong to different MeshBases' )

    if meshObject is None:
        ubos.logging.fatal( 'MeshObject does not exist:', meshObjectId )

    if neighbor is None:
        ubos.logging.fatal( 'Neighbor MeshObject does not exist:', neighborId )


    meshObject.blessRole( neighbor, args.roletypes );
    meshObject.save();

    ubos.logging.info( 'Blessed:', meshObject.getPath(), neighbor.getPath() )


def addSubParser( parentParser, cmdName ) :
    """
    Enable this command to add its own command-line options
    parentParser: the parent argparse parser
    cmdName: name of this command
    """
    parser = parentParser.add_parser( cmdName,   help='Bless the relationship between two already-related MeshObjects.' )
    parser.add_argument( 'meshobject',           help='The MeshObject')
    parser.add_argument( 'neighbor',             help='The neighbor MeshObject')
    parser.add_argument( 'roletypes', nargs='+', help='The RoleType(s) to bless with' )
