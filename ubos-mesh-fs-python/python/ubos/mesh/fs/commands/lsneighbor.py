#!/usr/bin/python
#
# List the neighbors of a MeshObject.
#
# Copyright (C) Johannes Ernst. All rights reserved. License: see package.
#

import argparse
import ubos.logging
import ubos.utils

from ubos.mesh.fs.MeshBase import MeshBase
from ubos.mesh.fs.MeshObject import MeshObject

def run( args ) :
    """
    Run this command.

    args: parsed command-line arguments
    """

    ( mb, meshObjectId ) = MeshBase.findMeshBase( args.meshobject )
    meshObject           = mb.findMeshObject( meshObjectId )

    if meshObject is None:
        ubos.logging.fatal( 'MeshObject does not exist:', meshObjectId )

    values = meshObject.neighborNames()

    for value in values :
        print( value )


def addSubParser( parentParser, cmdName ) :
    """
    Enable this command to add its own command-line options
    parentParser: the parent argparse parser
    cmdName: name of this command
    """
    parser = parentParser.add_parser( cmdName, help='List the neighbors of a MeshObject.' )
    parser.add_argument( 'meshobject',         help='The MeshObject' )
