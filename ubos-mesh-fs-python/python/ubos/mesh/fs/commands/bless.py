#!/usr/bin/python
#
# Bless an already-existing MeshObject.
#
# Copyright (C) Johannes Ernst. All rights reserved. License: see package.
#

import argparse
import ubos.logging
import ubos.utils

from ubos.mesh.fs.MeshBase import MeshBase
from ubos.mesh.fs.MeshObject import MeshObject

def run( args ) :
    """
    Run this command.

    args: parsed command-line arguments
    """

    ( mb, meshObjectId ) = MeshBase.findMeshBase( args.meshobject )
    meshObject           = mb.findMeshObject( meshObjectId )

    if meshObject is None:
        ubos.logging.fatal( 'MeshObject does not exist:', meshObjectId )

    meshObject.bless( args.entitytypes );
    meshObject.save();

    ubos.logging.info( 'Blessed:', meshObject.getPath() )


def addSubParser( parentParser, cmdName ) :
    """
    Enable this command to add its own command-line options
    parentParser: the parent argparse parser
    cmdName: name of this command
    """
    parser = parentParser.add_parser( cmdName,     help='Bless an already-existing MeshObject.' )
    parser.add_argument( 'meshobject',             help='The MeshObject')
    parser.add_argument( 'entitytypes', nargs='+', help='The EntityType(s) to bless with' )
