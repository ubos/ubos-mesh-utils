#!/usr/bin/python
#
# Delete a MeshObject.
#
# Copyright (C) Johannes Ernst. All rights reserved. License: see package.
#

import argparse
from pathlib import Path
import os
import ubos.logging
import ubos.utils

from ubos.mesh.fs.MeshBase import MeshBase
from ubos.mesh.fs.MeshObject import MeshObject

def run( args ) :
    """
    Run this command.

    args: parsed command-line arguments
    """

    for f in args.meshobjects :
        ( mb, meshObjectId ) = MeshBase.findMeshBase( f )
        meshObject           = mb.findMeshObject( meshObjectId )

        if meshObject is None:
            ubos.logging.fatal( 'MeshObject does not exist:', meshObjectId )

        p = Path( meshObject.getPath() )
        if p.is_dir() :
            os.rmdir( p )
        else :
            os.remove( p )

        ubos.logging.info( 'Deleted:', meshObject.getPath() )


def addSubParser( parentParser, cmdName ) :
    """
    Enable this command to add its own command-line options
    parentParser: the parent argparse parser
    cmdName: name of this command
    """
    parser = parentParser.add_parser( cmdName,      help='Delete one or more MeshObjects.' )
    parser.add_argument( 'meshobjects', nargs='+',  help='The MeshObject(s)')
