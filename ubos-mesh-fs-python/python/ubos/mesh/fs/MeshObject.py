#!/usr/bin/python
#
# A MeshObject in memory. This does not imply its existence in the
# filesystem.
#
# Copyright (C) Johannes Ernst. All rights reserved. License: see package.
#

from ubos.mesh.fs.consts import *
import hashlib
import os
from pathlib import Path
import re
import ubos.logging
import ubos.utils

class MeshObject :
    """
    Represents an an already-existing or to-be-created MeshObject.
    """
    def __init__( self, meshObjectId, mb ) :
        """
        Constructor.

        meshObjectId: Identifier of the MeshObject
        mb: reference to the owning MeshBase
        """
        self._meshObjectId = meshObjectId
        self._mb           = mb

        self._path   = mb.getRootDirWithSlash() + meshObjectId
        self._exists = None  # Don't know

        self._typesOnDisk   = None  # Don't know
        self._typesToAdd    = []
        self._typesToRemove = []

        self._propertyNamesOnDisk = None # Don't know
        self._propertiesOnDisk    = {}
        self._propertiesToChange  = {}

        self._neighborsToAdd    = []
        self._neighborsToRemove = []

        self._roleTypesOnDisk   = {} # None has no special value here. Keyed by neighbor Id, value is array
        self._roleTypesToAdd    = {}
        self._roleTypesToRemove = {}


    def getMeshObjectId( self ) :
        """
        Obtain the MeshObjectIdentifier
        """
        return self._meshObjectId


    def getMeshBase( self ) :
        """
        Obtain the MeshBase
        """
        return self._mb


    def getPath( self ) :
        """
        Obtain the path of the MeshObject in the filesystem
        """
        return self._path


    def exists( self ) :
        """
        Determine whether this MeshObject exists in the filesystem
        """
        if self._exists is None :
            self._exists = Path( self._path ).exists()
        return self._exists


    def bless( self, types ) :
        """
        Bless this MeshObject with these types.
        Only recorded, not performed until save().
        """
        ubos.logging.trace( 'Meshobject.bless', self, types )

        self._typesToAdd += types
        return self


    def unbless( self, types ) :
        """
        Unbless this MeshObject from these types.
        Only recorded, not performed until save().
        """
        ubos.logging.trace( 'Meshobject.unbless', self, types )

        self._typesToRemove += types
        return self


    def types( self ) :
        """
        Obtain the types this MeshObject is blessed with.
        """

        return self.futureTypes()


    def futureTypes( self ) :
        """
        Calculate the types of the MeshObject after the next save.
        """

        ret = []
        types = []

        if self._exists :
            self._readTypes()
            types += self._typesOnDisk

        types += self._typesToAdd

        for type in types :
            doAdd = True
            for remove in self._typesToRemove :
                if type == remove :
                    doAdd = False
                    break
            if doAdd :
                ret.append( type )

        return ret


    def isDirectory( self ) :
        """
        Determine whether this MeshObject should be represented as Directory
        in the filesystem
        """

        return self.isBlessedBy( DIRECTORY_TYPE )


    def isBlessedBy( self, type ) :
        """
        Determine whether this MeshObject is blessed with the provided type
        """

        futureTypes = self.futureTypes()
        if type in futureTypes :
            return True
        else :
            return False


    def getProperty( self, name ) :
        """
        Obtain the value of a property.
        """
        ubos.logging.trace( 'Meshobject.getProperty', self, name )

        return self.futurePropertyValueFor( name )


    def setProperty( self, name, value ) :
        """
        Set the value of a property
        """
        ubos.logging.trace( 'Meshobject.setProperty', self, name, value )

        self._propertiesToChange[name] = value


    def propertyNames( self ) :
        """
        Obtain the list of properties
        """
        self._readPropertyNames()

        return self._propertyNamesOnDisk
        # This may not be entirely correct because the MeshTypes of the MeshObject
        # may be changing at the same time, and we don't have access to the Model.
        # But we hope the this will not be invoked when types are changing during
        # the same invocation, too.


    def futurePropertyValueFor( self, name ) :
        """
        Calculate the value of one property after the next save.
        """
        self._readPropertyNames()

        if name in self._propertiesToChange :
            return self._propertiesToChange[name]
        else :
            return self._readPropertyValue( name )


    def relate( self, neighbor, roleTypes ) :
        """
        Relate two MeshObjects, and optionally bless the role
        """
        ubos.logging.trace( 'Meshobject.relate', self, neighbor, roleTypes )

        self._neighborsToAdd.append( neighbor._meshObjectId )
        if roleTypes is not None and len( roleTypes ) > 0 :
            if not neighbor._meshObjectId in self._roleTypesToAdd :
                self._roleTypesToAdd[ neighbor._meshObjectId ] = []

            self._roleTypesToAdd[ neighbor._meshObjectId ] += roleTypes

        return self


    def unrelate( self, neighbor ) :
        """
        Unrelate two MeshObjects.
        """
        ubos.logging.trace( 'Meshobject.unrelate', self, neighbor )

        self._neighborsToRemove.append( neighbor._meshObjectId )

        return self


    def neighborNames( self ) :
        """
        Determine the neighbors of a MeshObject.
        """
        ubos.logging.trace( 'Meshobject.neighborNames', self )

        dir = self._directory();
        if not Path( dir ).is_dir() :
            return []

        ret = []
        for entry in os.listdir( dir ) :
            fullEntry = dir + '/' + entry
            if Path( fullEntry ).is_symlink() :
                dest = ubos.utils.absReadLink( fullEntry )
                dest = dest.replace( '\s', '' ) # \n in the file

                # dest = Path( dest ).relative_to( dir )
                dest = self._mb.getPathToIdentifier( dest )

                ret.append( dest )
        return ret


    def blessRole( self, neighbor, roleTypes ) :
        """
        Bless a role between two existing, related MeshObjects
        """
        ubos.logging.trace( 'Meshobject.blessRole', self, neighbor, roleTypes )

        if not neighbor._meshObjectId in self._roleTypesToAdd :
            self._roleTypesToAdd[ neighbor._meshObjectId ] = []

        self._roleTypesToAdd[ neighbor._meshObjectId ] += roleTypes
        return self


    def unblessRole( self, neighbor, roleTypes ) :
        """
        Unbless a role between two existing, related MeshObjects
        """
        ubos.logging.trace( 'Meshobject.unblessRole', self, neighbor, roleTypes )

        if not neighbor._meshObjectId in self._roleTypesToRemove :
            self._roleTypesToRemove[ neighbor._meshObjectId ] = []

        self._roleTypesToRemove[ neighbor._meshObjectId ] += roleTypes
        return self


    def roleTypes( self, neighbor ) :
        """
        Determine the role types between this and another MeshObject.
        """
        ubos.logging.trace( 'Meshobject.roleTypes', self, neighbor )

        return self.futureRoleTypes( neighbor )


    def futureRoleTypes( self, neighbor ) :
        """
        Calculate the role types between this and another MeshObject after the next save.
        """
        ubos.logging.trace( 'Meshobject.futureRoleTypes', self, neighbor )

        self._readRoleTypes( neighbor )

        ret             = []
        addRoleTypes    = []
        removeRoleTypes = []

        if neighbor._meshObjectId in self._roleTypesOnDisk :
            addRoleTypes += self._roleTypesOnDisk[ neighbor._meshObjectId ]

        if neighbor._meshObjectId in self._roleTypesToAdd :
            addRoleTypes += self._roleTypesToAdd[ neighbor._meshObjectId ]

        if neighbor._meshObjectId in self._roleTypesToRemove :
            removeRoleTypes += self._roleTypesToRemove[ neighbor._meshObjectId ]

        for roleType in addRoleTypes :
            doAdd = True
            for remove in removeRoleTypes :
                if roleType == remove :
                    doAdd = False
                    break

            if doAdd :
                ret.append( roleType )

        return ret


    def save( self ) :
        """
        Save this MeshObject to disk / the UBOS Data Mesh filesystem
        """
        ubos.logging.trace( 'Meshobject.save', self )

        myDir = self._directory()

        # create file if needed
        if not Path( self._path ).exists() :
            if self.isDirectory() :
                ubos.utils.mkdir( self._path )
            else :
                ubos.utils.saveFile( self._path, '' )

        # bless / unbless
        if len( self._typesToAdd ) > 0 or len( self._typesToRemove ) > 0 :
            types       = self.futureTypes()
            typesString = ' '.join( types )

            self._setxattr( self._path, MESH_OBJECT_TYPES_XATTR_NAME, typesString.encode( 'utf-8' ))

        # new property values
        for propertyName in self._propertiesToChange :
            propertyValue = self._propertiesToChange[propertyName]

            if propertyValue is None :
                propertyValue = NULL_VALUE;

            self._setxattr( self._path, MESH_OJBECT_PROPERTY_XATTR_PREFIX + propertyName, propertyValue.encode( 'utf-8' ))

        # new / deleted relationships
        for neighborId in self._neighborsToRemove :
            neighbor     = self._mb.findMeshObject( neighborId )
            neighborHash = neighbor._meshObjectIdHash();

            if Path( myDir + '/' + neighborHash ).is_file() :
                ubos.utils.deleteFile( myDir + '/' + neighborHash + NEIGHBOR_LINK_POSTFIX )

        for neighborId in self._neighborsToAdd :
            neighbor     = self._mb.findMeshObject( neighborId )
            neighborHash = neighbor._meshObjectIdHash();

            if not Path( myDir ).is_dir() :
                ubos.utils.mkdir( myDir ) # when running on a regular file system, not UBOS Data Mesh FS

            if not Path( myDir + '/' + neighborHash ).is_file() :
                ubos.utils.symlink( self._relativePath( neighbor ), myDir + '/' + neighborHash + NEIGHBOR_LINK_POSTFIX )

        # bless RoleType / unbless RoleType
        affectedNeighbors = {};
        for neighborId in self._roleTypesToAdd :
            affectedNeighbors[neighborId] = 1
        for neighborId in self._roleTypesToRemove :
            affectedNeighbors[neighborId] = 1

        for neighborId in affectedNeighbors :
            neighbor     = self._mb.findMeshObject( neighborId )
            neighborHash = neighbor._meshObjectIdHash()

            if Path( myDir + '/' + neighborHash ).exists() :
                roleTypes       = self.futureRoleTypes( neighbor )
                roleTypesString = ' '.join( roleTypes )

                self._setxattr( myDir + '/' + neighborHash, MESH_OBJECT_ROLE_TYPES_XATTR_NAME, roleTypesString.encode( 'utf-8' ))

        return self


    def _readTypes( self ) :
        """
        Helper method to read the EntityTypes that this MeshObject is blessed
        with from the filesystem.
        """
        if self._typesOnDisk is None :
            ubos.logging.trace( 'Meshobject._readTypes', self )

            if Path( self._path ).exists() :
                types = os.getxattr( self._path, MESH_OBJECT_TYPES_XATTR_NAME ).decode("utf-8")

                if types :
                    self._typesOnDisk = re.split( '\s+', types )
                else :
                    self._typesOnDisk = []

            else :
                ubos.logging.error( 'MeshObject does not exist:', self )

        return self._typesOnDisk


    def _readPropertyNames( self ) :
        """
        Helper method to read the names of the Properties that this MeshObject carries
        from the filesystem (not the values).
        """
        if self._propertyNamesOnDisk is None :
            ubos.logging.trace( 'Meshobject._readPropertyNames', self )

            if Path( self._path ).exists() :
                # Issue: the following call:
                #     names = os.listxattr( self._path )
                # returns only a subset of the actual properties that are there.
                # Why, I don't know. The properties are there, as checked with
                # attr from the command-line and a test C program using listxattr
                # in C. So we use this clumsy workaround.
                result = ubos.utils.myexec( "getfattr -d '%s'" % self._path, captureStdout=True, captureStderr=True )
                lines  = result[1].decode('utf-8').split( '\n' )
                names  = []
                for line in lines :
                    if line and not line.startswith( '#' ):
                        m = re.search( '^(.*)=', line )
                        if m :
                            names.append( m.group( 1 ))
                        else :
                            ubos.logging.error( "Does not match:", line )

                self._propertyNamesOnDisk = []

                for name in names :
                    if name.startswith( MESH_OJBECT_PROPERTY_XATTR_PREFIX ) :
                        self._propertyNamesOnDisk.append( name[ len( MESH_OJBECT_PROPERTY_XATTR_PREFIX ) : ] )

            else :
                ubos.logging.error( 'MeshObject does not exist:', self )

        return self._propertyNamesOnDisk


    def _readPropertyValue( self, name ) :
        """
        Helper method to read the value of a named Property that this MeshObject carries
        from the filesystem
        """
        if name in self._propertiesOnDisk :
            ret = self._propertiesOnDisk[name]
        else :
            ubos.logging.trace( 'Meshobject._readPropertyValue', self, name )

            if Path( self._path ).exists() :
                value = os.getxattr( self._path, MESH_OJBECT_PROPERTY_XATTR_PREFIX + name ).decode("utf-8")
                self._propertiesOnDisk[ name ] = value
                ret = value

            else :
                ubos.logging.error( 'MeshObject does not exist:', self )
                ret = None

        return ret


    def _readRoleTypes( self, neighbor ) :
        """
        Helper method to read the RoleTypes that this MeshObject participates in
        with a neighbor MeshObject from the filesystem.
        """
        if not neighbor._meshObjectId in self._roleTypesOnDisk :
            ubos.logging.trace( 'Meshobject._readRoleTypes', self, neighbor )

            neighborHash = neighbor._meshObjectIdHash()
            link         = self._directory() + '/' + neighborHash

            if Path( link ).exists() :
                types = os.getxattr( link, MESH_OBJECT_ROLE_TYPES_XATTR_NAME ).decode("utf-8")
                if types :
                    self._roleTypesOnDisk[ neighbor._meshObjectId ] = re.split( '\s+', types )

            else :
                ubos.logging.error( 'Not related' );
                self._roleTypesOnDisk[ neighbor._meshObjectId ] = []

        return self


    def _directory( self ) :
        """
        Determine the UBOS Data Mesh FS directory that goes with this MeshObject.
        """
        ubos.logging.trace( 'Meshobject._directory', self )

        ret = self._mb.getRootDirWithSlash();
        slash = self._meshObjectId.find( '/' )
        if slash >= 0 :
            ret = self._meshObjectId[ 0 : slash ] + '/' + REL_DIR_PREFIX + self._meshObjectId[ slash : ]
        else :
            ret = REL_DIR_PREFIX + self._meshObjectId

        return ret


    def _meshObjectIdHash( self ) :
        """
        Determine the hash of an identifier
        """
        ubos.logging.trace( 'Meshobject._meshObjectIdHash', self )

        ret = hashlib.sha256( self._meshObjectId.encode('utf-8') ).hexdigest()

        return ret


    def _relativePath( self, neighbor ) :
        """
        Determine a relative path from the symlink in the relationship directory
        for this MeshObject to the file representing the neighbor
        """
        ubos.logging.trace( 'Meshobject._relativePath', self, neighbor )

        one = self._meshObjectId.split( '/' )
        two = neighbor._meshObjectId.split( '/' )

        i = 0
        while i < len( one ) and i < len( two ) and one[i] == two[i] :
            i += 1

        ret  = '../' * ( len( one ))
        ret += '/'.join( two )
        return ret


    def _setxattr( self, path, xattrName, xattrValue ) :
        """
        Wrapper function for setxattr for consistent error handling
        """
        try :
            status = os.setxattr( path, xattrName, xattrValue )
            if status :
                ubos.logging.error( 'setxattr failed for', path, ':', xattrName, '->', xattrValue, ' with status:', status )
            else :
                return True

        except OSError as e:
            ubos.logging.error( 'setxattr exception for', path, ':', xattrName, '->', xattrValue, ' with message:', e.strerror )

        return False

