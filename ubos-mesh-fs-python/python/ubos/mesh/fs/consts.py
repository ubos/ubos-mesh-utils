#!/usr/bin/python
#
# Some definitions.
#
# Copyright (C) Johannes Ernst. All rights reserved. License: see package.
#

ROOT_DIR_MARKER = '.ubos-mesh-fs'
EXT_ATTR_PREFIX = 'user.ubos.mesh.'
MESH_OBJECT_TYPES_XATTR_NAME      = EXT_ATTR_PREFIX + 'types'
MESH_OJBECT_PROPERTY_XATTR_PREFIX = EXT_ATTR_PREFIX + 'property.'
MESH_OBJECT_ROLE_TYPES_XATTR_NAME = EXT_ATTR_PREFIX + 'roletypes'

DIRECTORY_TYPE        = 'net.ubos.model.Filesystem/Directory'
FILE_TYPE             = 'net.ubos.model.Filesystem/File'
LOCAL_FILE_PREFIX     = '.ubos-mesh-fs'
REL_DIR_PREFIX        = LOCAL_FILE_PREFIX + '-rel-'
NULL_VALUE            = '<NULL>'
NEIGHBOR_LINK_POSTFIX = '.link'
