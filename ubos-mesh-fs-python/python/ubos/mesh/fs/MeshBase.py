#!/usr/bin/python
#
# A MeshBase.
#
# Copyright (C) Johannes Ernst. All rights reserved. License: see package.
#

from ubos.mesh.fs.consts import *
from ubos.mesh.fs.MeshObject import MeshObject
from pathlib import Path
import ubos.logging
import ubos.utils

class MeshBase :
    """
    A MeshBase in the filesystem
    """

    knownMeshBases = {} # Maps rootDir to MeshBase instance


    def findMeshBase( pathToMeshObject ) :
        """
        Given the provided path to a MeshObject, and considering the context
        directory, determine the location of a MeshBase (if any), and if there
        is one, instantiate it. Otherwise fatal out. This is a factory method.

        pathToMeshObject: filename supposedly identifying a MeshObject in the file system
        return: ( MeshBase, canonicalized MeshObjectIdentifier )
        """
        ubos.logging.trace( 'MeshBase.findMeshBase', pathToMeshObject )

        absPathToMeshObject = str( Path( pathToMeshObject ).resolve() )
        absPathComponents   = absPathToMeshObject.split( '/' )

        rootDir      = None;
        meshObjectId = None;

        for i in range( len(absPathComponents), 1, -1 ) :
            candidate = '/'.join( absPathComponents[:i] )

            if Path( candidate + '/' + ROOT_DIR_MARKER ).is_dir() :
                rootDir = candidate
                meshObjectId = ''
                sep          = ''

                for j in range( i, len(absPathComponents) ) :
                    meshObjectId += sep + absPathComponents[j]
                    sep = '/'

                break

        if rootDir is None:
            ubos.logging.fatal( 'Cannot find a MeshBase for:', pathToMeshObject )


        if rootDir in MeshBase.knownMeshBases :
            # reuse existing MeshBase
            self = MeshBase.knownMeshBases[rootDir]

        else:
            self = MeshBase( rootDir )
            MeshBase.knownMeshBases[rootDir] = self

        return ( self, meshObjectId )


    def __init__( self, rootDir ) :
        """
        rootDir: the root directory of the MeshBase in the filesystem
        """
        self.rootDir = rootDir
        self.rootDirWithSlash = rootDir + '/'
        self.meshObjects = {}


    def findMeshObjectFromFile( self, pathToMeshObject ) :
        """
        Given a the name of a file representing a MeshObject in this MeshBase,
        return the MeshObject

        pathToMeshObject: filename supposedly identifying a MeshObject in the file system
        return: the MeshObject, or None
        """
        ubos.logging.trace( 'MeshBase.findMeshObjectFromFile', pathToMeshObject )

        absPathToMeshObject = Path( pathToMeshObject ).resolve()

        if absPathToMeshObject.startswith( self.rootDirWithSlash ) :
            meshObjectId = absPathToMeshObject[ len( self.rootDirWithSlash ) : ]
            return self.findMeshObject( meshObjectId )

        else:
            ubos.logging.error( 'Path not in this MeshBase:', pathToMeshObject, self )
            return None


    def findMeshObject( self, meshObjectId ) :
        """
        Given this canonical MeshObjectId, find the corresponding MeshObject (or None)

        meshObjectId: the canonical MeshObjectId
        return: the MeshObject, or None
        """
        ubos.logging.trace( 'MeshBase.findMeshObject', meshObjectId )

        if meshObjectId in self.meshObjects :
            return self.meshObjects[meshObjectId]

        ret = MeshObject( meshObjectId, self )
        if not ret.exists() :
            ret = None

        self.meshObjects[meshObjectId] = ret
        return ret


    def createMeshObject( self, meshObjectId, bless = None ) :
        """
        Create a MeshObject with with this canonical MeshObjectId.

        meshObjectId: the canonical MeshObjectId
        bless: optional array of MeshTypes to bless this MeshObject with
        return: the MeshObject
        """
        ubos.logging.trace( 'MeshBase.createMeshObject', meshObjectId, bless )

        ret = self.findMeshObject( meshObjectId )
        if ret is not None:
            raise MeshObjectIdentifierNotUniqueException( ret )

        ret = MeshObject( meshObjectId, self )
        if bless is not None and len( bless ) > 0 :
            ret.bless( bless );

        self.meshObjects[meshObjectId] = ret
        return ret


    def getRootDir( self ) :
        """
        Obtain the root directory of this MeshBase.

        return: the root directory
        """
        return self.rootDir


    def getRootDirWithSlash( self ) :
        """
        Obtain the root directory of this MeshBase, with appended slash.

        return: the root directory
        """
        return self.rootDirWithSlash


    def getPathToIdentifier( self, path ) :
        """
        Given an absolute path to a file representing a MeshObject in this MeshBase,
        return the MeshObject's identifer.

        path: the absolute path
        return: the identifer, or undef if not in this MeshBase
        """
        if path.startswith( self.rootDirWithSlash ) :
            return path[ len( self.rootDirWithSlash ) : ]
        else :
            ubos.logging.error( 'Path not in this MeshBase:', path, self )
            return undef

